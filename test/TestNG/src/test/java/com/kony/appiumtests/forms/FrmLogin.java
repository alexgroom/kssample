/**************************************************************
Project Name			:	Test Automation Sample
Package Name			:	com.kony.appiumtests.Forms
Class Name				:	Login
Purpose of the Class	:	To maintain the repository for the locators
 **************************************************************/

package com.kony.appiumtests.forms;

import io.appium.java_client.remote.HideKeyboardStrategy;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class FrmLogin extends BaseForm {

	/**
	 * The page structure is being used within this test in order to separate
	 * the page actions from the tests
	 * 
	 * Locators are saved with the help of @FindBy annotation, and can be used
	 * in the corresponding tests by extending the FrmHome class.
	 * 
	 * LoginIn() method is used to Login into the application. we can pass the
	 * user name, and password.
	 */

	public FrmLogin(RemoteWebDriver driver) {
		super(driver); // super() is used to invoke immediate parent class
						// constructor.
	}

	// Locators_PreLogin Screen
	/**
	 * @FindBy is just an alternate way of finding elements. It is better used
	 *         to support the PageObject pattern.
	 */

	private WebElement lbl_SignIn;
	
	@iOSFindBy(accessibility = "Go to frm1")
	public WebElement lbl_frm1;
	
	@iOSFindBy(accessibility = "Go to frm2")
	public WebElement lbl_frm2;
	
	@iOSFindBy(accessibility = "Go to frm3")
	public WebElement lbl_frm3;
	
	@iOSFindBy(accessibility = "Go to frm4")
	public WebElement lbl_frm4;
	
	@iOSFindBy(accessibility = "Go to frm5")
	public WebElement lbl_frm5;
	
	@iOSFindBy(accessibility = "Go to frmLogout")
	public WebElement lbl_frmLogout;

	private WebElement username;

	private WebElement pass_word;

	private void initiaizeElements(){
		System.out.println("textBoxClass for the platform .."+platformName+".. is ... "+textBoxClass);
		if ("LINUX".equalsIgnoreCase(platformName)) 
		{
		List<WebElement> textBoxList = driver.findElements(By.className(textBoxClass));
		this.username = textBoxList.get(0);
		this.pass_word = textBoxList.get(1);
		}
		else
		{
		try
		{
			this.username = driver.findElement(By.name("TextField1"));
			System.out.println("Found by name TextField1 " + this.username.toString());
			
			this.pass_word = driver.findElement(By.name("TextField2"));
			System.out.println("Found by name TextField2 " + this.pass_word.toString());

			this.btnLogin = driver.findElement(By.name("btnSignIn"));
			System.out.println("Found by name for button " + this.btnLogin.toString());

			this.lbl_SignIn = driver.findElement(By.name("lblsignin"));
			System.out.println("Found by name for lbl " + this.lbl_SignIn.toString());
		} catch(Exception e) 
		{
			System.out.println("Failed to find element on MAC " + e.toString());
		}

		}
		
	}



	@AndroidFindBy(className = "android.widget.Button")	
	public WebElement btnLogin;

	public void loginIn(String userName, String password) 
	{
		this.printElements();
		this.initiaizeElements();

	
		try
		{
			WebElement we = driver.findElement(By.className(textBoxClass));
			System.out.println("Found we textBoxClass " + we.toString());
		} catch(Exception e) {}

		try
		{
			WebElement we = driver.findElement(By.name("TextField1"));
			System.out.println("Found we by name TextField1 " + we.toString());
		} catch(Exception e) {}

		try
		{
			WebElement we = driver.findElement(By.tagName("XCUIElementTypeTextField"));
			System.out.println("Found we by tag name XCUIElementTypeTextField " + we.toString());
		} catch(Exception e) {}
		
		try
		{
			WebElement we = driver.findElementByName("TextField1");
			System.out.println("Found we by name2 " + we.toString());
		} catch(Exception e) {}
		
		try
		{
			WebElement we = driver.findElement(By.tagName("XCUIElementTypeButton"));
			System.out.println("Found we by tag name for button " + we.toString());
		} catch(Exception e) {}

		try
		{
			WebElement we = driver.findElement(By.name("btnSignIn"));
			System.out.println("Found we by name for button " + we.toString());
		} catch(Exception e) {}
		
		
		this.username.sendKeys(userName);
		if ("MAC".equalsIgnoreCase(platformName)) 
		{
			iosdriver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "Done");
		} else {
			androiddriver.hideKeyboard();
		}
		this.pass_word.sendKeys(password);
		if ("MAC".equalsIgnoreCase(platformName)) {
			iosdriver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "Done");
		} else {
			androiddriver.hideKeyboard();
		}
		this.btnLogin.click();
	}

	/**
	 * isDisplayed() is boolean method i.e, it returns true or false. Basically
	 * this method is used to find whether the element is being displayed.
	 */
	public boolean isDisplayed() {
		return (this.lbl_SignIn.isDisplayed() && this.btnLogin.isDisplayed());
	}

}
